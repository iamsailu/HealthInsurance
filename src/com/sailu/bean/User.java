package com.sailu.bean;


public class User 

{
	int Id;
	String name;
	int age;
	String gender;
	HealthProblems healthproblems;
	Badhabits badhabits;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public HealthProblems getHealthproblems() {
		return healthproblems;
	}
	public void setHealthproblems(HealthProblems healthproblems) {
		this.healthproblems = healthproblems;
	}
	public Badhabits getBadhabits() {
		return badhabits;
	}
	public void setBadhabits(Badhabits badhabits) {
		this.badhabits = badhabits;
	}
	
	
	

}
