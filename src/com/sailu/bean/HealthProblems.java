package com.sailu.bean;

public class HealthProblems 
{
	private String  hypertension;
	private String blood_Pressure;
	private String blood_Sugar;
	private String over_weight;
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBlood_Pressure() {
		return blood_Pressure;
	}
	public void setBlood_Pressure(String blood_Pressure) {
		this.blood_Pressure = blood_Pressure;
	}
	public String getBlood_Sugar() {
		return blood_Sugar;
	}
	public void setBlood_Sugar(String blood_Sugar) {
		this.blood_Sugar = blood_Sugar;
	}
	public String getOver_weight() {
		return over_weight;
	}
	public void setOver_weight(String over_weight) {
		this.over_weight = over_weight;
	}
	
	
	

}
