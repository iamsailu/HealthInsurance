package com.sailu.controllers;

import java.util.Map;



import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.sailu.bean.User;
import com.sailu.dao.healthpayment;



@Controller
@RequestMapping("/registrationform.html")
public class HealthinsurenceController {
	
	public User user;
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showRegistration(Map model) {
		User user = new User();
	
		model.put("registration", user);
		return "UserForm";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String processForm(User user, BindingResult result,Map model) {
		String name = "name";
		String gender = "gender";
		String age="age";
		String healthproblems="healthproblems";
		String badhabits="badhabits";
		
		healthpayment health = new healthpayment() {
			
			@Override
			public void setAmount(int amount) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public int getAmount(int amount) {
				// TODO Auto-generated method stub
				return 0;
			}
		};
		health.setAmount(5000);

		double basicAmount = health.getAmount(5000);
		double calculation = 0;
		
		if (getUser().getAge() <= 18) {
			System.out.println("Below 18 Years" + " " + getUser().getName() + " " + health.getAmount(5000));
		} else if (getUser().getAge() > 18 && getUser().getAge() <= 25) {
			calculation = basicAmount + (basicAmount * 10) / 100;
		} else if (getUser().getAge() > 25 && getUser().getAge() <= 30) {
			calculation = basicAmount + (basicAmount * 10) / 100;
		} else if (getUser().getAge() > 30 && getUser().getAge() <= 35) {
			calculation = basicAmount + (basicAmount * 10) / 100;
		} else if (getUser().getAge() > 35 && getUser().getAge() <= 40) {
			calculation = basicAmount + (basicAmount * 10) / 100;
		} else {
			calculation = basicAmount + (basicAmount * 20) / 100;
		}

		
		if (getUser().getGender() == "Male") {
			calculation = calculation + (calculation * 2) / 100;
		} else {
			calculation = basicAmount;
		}

		
		if (getUser().getHealthproblems().equals(healthproblems))
		{
			calculation = calculation + (calculation * 1) / 100;
		} 
		else if (getUser().getHealthproblems().equals(badhabits)) {
			calculation = calculation + (calculation * 1) / 100;
		} else if (getUser().getHealthproblems().equals(health)) {
			calculation = calculation + (calculation * 1) / 100;
		} else if (getUser().getHealthproblems().equals(healthproblems)) {
			calculation = calculation + (calculation * 1) / 100;
		}

		

		if (getUser().getBadhabits().equals(badhabits)) {
			calculation = calculation - (calculation * 3) / 100;
		} else if (getUser().getBadhabits().equals(badhabits)) {
			calculation = calculation + (calculation * 3) / 100;
		} else if (getUser().getBadhabits().equals(badhabits)) {
			calculation = calculation + (calculation * 3) / 100;
		} else if (getUser().getBadhabits().equals(badhabits)) {
			calculation = calculation + (calculation * 3) / 100;
		}
		System.out.println("calculation ::" + calculation);
		
		return "UserHealthSheet.jsp";

	}

	}


